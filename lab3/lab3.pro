TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp

win32 {
LIBS += -Lc:/OpenCV/bin -Lc:/OpenCV/lib -lopencv_core242 -lopencv_highgui242 \
-lopencv_imgproc242 -lopencv_features2d242 -lopencv_calib3d242
INCLUDEPATH += c:/OpenCV/include
}
