#include <iostream>
#include "opencv/cv.h"
#include "opencv/highgui.h"

using namespace cv;


int main( int argc, char** argv )
{
    Mat image;
    double duration;

    image = imread(argv[1],1);
    if(!image.data)
    {
        std::cout<<"no image"<<std::endl;
        return 1;
    }

    //feature detection begins here
      duration = static_cast<double>( cv::getTickCount() );

      std::vector<KeyPoint> keypoints;

      GFTTDetector detector(2894);

    detector.detect(image,keypoints);

    Mat key_image;

    drawKeypoints(image,keypoints,key_image,Scalar::all(-1));



    //feature detection ends here.



    // Your time-consuming code
    duration = (static_cast<double>( cv::getTickCount() ) - duration) / cv::getTickFrequency();
    std::cout << "Duration: " << duration << " s" << std::endl;


    namedWindow("window 1");
    imshow("window 1",key_image);
    imwrite("image_GFTT_Selection.jpg",key_image);
    waitKey(0);
    return 0;



}



